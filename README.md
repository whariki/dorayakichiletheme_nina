Dorayaki Child Theme for Nina.  
================
This is a child theme for wordpress and requires the Dorayaki Theme from http://www.elmastudio.de/

The child theme is for www.extrashotmarketing.co.nz
____

## Modifiers
The child theme modifies the header to allow for a bigger logo... Had to do it to allow readable text in the logo.
